# coding: utf8
# Copyright: MathDecision


"""

Un flujo f es una función de las aristas de un grafo a los reales positivos y que
satisface la ecuación de balance en cada vértice 'interior' del grafo.
Los vértices 'interiores' son los que son diferentes a un conjunto preseleccionado
('la frontera'), el cual puede ser vacio. En tal caso, decimos que f es un flujo
con frontera S.

Una representación posible de un flujo es un diccionario que tiene como claves
las aristas del grafo.

"""

from combopt.graph import Grafo
from combopt.minpath import minimum_directed_path
from combopt.utils import inv


def is_flow(f, G, S, cap=None):
    """ Verifica que la función f que va de las aristas de G en R sea efectivamente
    un flujo con frontera S.

    Si cap no es None, verifica además que el flujo cumpla las condiciones
    de capacidad especificadas por la función cap

    :param f:
    :type f: dict
    :param G:
    :type G: Grafo
    :param S:
    :type S: set
    """

    assert G.directed

    # Condición de positividad
    for e in G.aristas:
        if f[e] < 0:
            return False

    # Condición de capacidad
    if cap is not None:
        for e in G.aristas:
            if f[e] > cap[e]:
                return False

    # Condición de conservación
    for v in G.vertices:
        if v not in S:
            inflow = sum(f[(w, v)] for w in G.predecessors(v))
            outflow = sum(f[(v, w)] for w in G.succesors(v))
            if inflow != outflow:
                return False

    return True


def residual_graph(f, c, G):
    """ Crea un nuevo grafo Df con los mismos vértices de G, y tal que, dada
    una arista e en G:
    - e pertenece a Df si f(a) < c(a)
    - e^(-1) pertenece a Df si f(a) > 0
    """
    vertices = G.vertices
    aristas = []
    for e in G.aristas:
        if f[e] < c[e]:
            aristas.append(e)
        if f[e] > 0:
            aristas.append(inv(e))
    return Grafo(vertices, aristas, directed=True)


def _path_directions(G, P):
    """ Esta función toma un path no dirigido en G y nos retorna una función que,
    para cada arista de P nos da su dirección en P.
    Ejemplo, Supongamos las aristas de G son (1, 2), (3, 2), (4, 3), (4, 5), (2, 4).
    P = [1, 2, 3, 4, 5] es un camino no dirigido cuyas aristas son.
    (1, 2), (3, 2), (4, 3) y (4, 5). Las direcciones de estas aristas en P, son:
    1, -1, -1 y 1, respectivamente.
    Se asume que G no tiene ciclos de longitud 1 o 2.
    """
    phi = dict()
    for u, v in zip(P[:-1], P[1:]):
        if (u, v) in G.aristas:
            phi[(u, v)] = 1
        elif (v, u) in G.aristas:
            phi[(v, u)] = -1
    return phi


def improve_flow(f, c, G, P):
    """ Esta función toma un path no dirigido en G, de s a t y nos retorna la mejor
    modificación de f sobre el camino P.
    """
    phi = _path_directions(G, P)
    epsilon = float('inf')
    for e, signo in phi.items():
        if signo == 1:
            epsilon = min(epsilon, c[e] - f[e])
        else:
            epsilon = min(epsilon, f[e])
    for e, signo in phi.items():
        f[e] = f[e] + signo * epsilon
    return f, epsilon



# def residual_action(f, c, G):
#     """ Modifica el grafo G, para obtener un nuevo grafo Df con los mismos
#     vértices de G, y tal que, dada una arista e en G:
#     - e pertenece a Df si f(a) < c(a)
#     - e^(-1) pertenece a Df si f(a) > 0
#     """
#     vertices = G.vertices
#     for e in G.aristas:
#         if f[e] < c[e]:
#             aristas.append(e)
#         if f[e] > 0:
#             aristas.append(inv(e))
#     return Grafo(vertices, aristas, directed=True)


def maximum_flow(grafo, cap, s, t):
    """

    :param grafo:
    :type grafo: Grafo
    :param cap:
    :return:
    """

    # Partimos de un flujo trivial

    # Calculamos el residual respecto a ese flujo

    # Hallamos un camino de s a t en el residual

    # Si no hay tal camino, terminamos

    # De otra forma, mejoramos el flujo y repetimos

    f = {e: 0 for e in grafo.aristas}
    while True:
        residual = residual_graph(f, cap, grafo)
        try:
            path = minimum_directed_path(residual, s, t)
        except ValueError:
            return f
        f, eps = improve_flow(f, cap, grafo, path)
